#include "ipc.h"

#ifndef _CHILD_H
#define _CHILD_H

typedef struct {
    int* read_fd;
    int* write_fd;
    int processes_count;
    local_id id;
} MessageAttributes;


typedef struct 
{
    int in;
    int out;
} PipeFd;
#endif
