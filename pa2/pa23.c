#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include "banking.h"
#include "common.h"
#include "ipc.h"
#include "pa2345.h"
#include "child.h"
#include <getopt.h>
#include <sys/wait.h>
#include <time.h>

#define SUCCESS 0
#define FAILURE 1
#define PARENT_ID 0
#define ANY_CHILD -1
#define READ_FD 0
#define WRITE_FD 1

static int processes_count = 0;
static FILE  *pipeslog;
static int eventslog;
static int connections_count = 0;
static int** read_fd;
static int** write_fd;
static balance_t balance;
static BalanceHistory history;

void close_unused_fd(local_id process_id);
int run_child(int* write_fd, int* read_fd, local_id child_id);
int synchronize(int* write_fd, int* read_fd, local_id child_id, MessageType m_type);
int do_transactions(int* write_fd, int* read_fd, local_id child_id);
void close_descriptors();
void free_descriptors();
void suicide();
int run_parent();

void transfer(void *self, local_id src, local_id dst, balance_t amount)
{

    TransferOrder order;
    order.s_src = src;
    order.s_dst = dst;
    order.s_amount = amount;
 
    Message message;
    message.s_header.s_magic = MESSAGE_MAGIC;
    message.s_header.s_type = TRANSFER;
    message.s_header.s_payload_len = sizeof(order);
    memcpy(message.s_payload, &order, sizeof(order));
    message.s_header.s_local_time = get_physical_time();

    send(self, src, &message);
 
    //getting ACK
    do
    {
        receive(self, dst, &message);
    }
    while (message.s_header.s_type != ACK);
}

int main(int argc, char **argv)
{
    char option;
    balance_t initial_balances[10];
    int flag_is_found = 0;
    int length = 0;
    int fields[2];
    int flags;
    //Parsing command line arguments.
    while ((option = getopt(argc, argv, "p:")) != -1)
    {
       	switch (option)
      	{
          	case 'p':
            	length = strlen(optarg);
                for (int i = 0; i < length; i++)
                if (!isdigit(optarg[i]))
                {
                     exit(1);
                }
                processes_count = atoi(optarg);
                flag_is_found = 1;
                if (processes_count > 10 || processes_count < 1)
                {
                    exit(1);
                }
                break;
            case '?': 
                exit(1);
        }
    }
    //Exiting if -p flag not found
    if (!flag_is_found)
    {
        exit(FAILURE);
    }
    //Checking for number of arguments
    if (argc !=  processes_count + 3)
    {
        exit(FAILURE);
    }
    //Reading start balances
    for (int i = 0; i < processes_count; i++) {
       initial_balances[i] = atoi(argv[i + 3]);
    }
    //Creating files to logging
    eventslog = open(events_log, O_CREAT | O_TRUNC |  O_RDWR | O_APPEND, S_IRWXU | S_IRWXG | S_IRWXO);
    pipeslog = fopen(pipes_log, "w");

    //Creating files descriptors
    connections_count = processes_count + 1;
    read_fd = malloc(sizeof(int*)*(connections_count));
    write_fd = malloc(sizeof(int*)*(connections_count));
    for (int i=0; i<connections_count; ++i)
    {
        read_fd[i] = malloc(sizeof(int)*(connections_count));
        write_fd[i] = malloc(sizeof(int)*(connections_count));
    }

    //Creating files descriptors
    for (int i=0; i<connections_count; ++i)
    {
        for (int j=0; j<connections_count; ++j)
        {
            if (i == j) { continue; }
            if (SUCCESS == pipe(fields))
            {
                flags = fcntl(fields[READ_FD], F_GETFL,0);
                fcntl(fields[READ_FD], F_SETFL, flags | O_NONBLOCK);
                flags = fcntl(fields[WRITE_FD], F_GETFL,0);
                fcntl(fields[WRITE_FD], F_SETFL, flags | O_NONBLOCK);
                read_fd[j][i] = fields[READ_FD];
                write_fd[i][j] = fields[WRITE_FD];
                fprintf(pipeslog, "%d-->%d %d/%d\n", i,j, fields[WRITE_FD], fields[READ_FD]);
            }
            else
            {
                free_descriptors();
                exit(FAILURE);
            }
        }
    }
    //Saving pipes to log
    fclose(pipeslog);

    //Starting to create children
    pid_t *child_processes = malloc(sizeof(pid_t) * processes_count);
    for (int i = 0; i < processes_count; i++)
    {
        child_processes[i] = fork();
        int childId = i + 1;
        //Exit if failed to fork
        if (-1 == child_processes[i] )
        {
                free_descriptors();
                exit(FAILURE);
        }
        //Child logic here
        if (0 ==  child_processes[i])
        {
            balance = initial_balances[i];
            timestamp_t time = get_physical_time();
            history.s_id = childId;
            history.s_history_len = time + 1;
            for (int i = 0; i <= time; i++)
            {
                history.s_history[i].s_balance = (i == time ? balance : 0);
                history.s_history[i].s_balance_pending_in = 0;
                history.s_history[i].s_time = i;
            }
            //close other child FD
            close_unused_fd((local_id)childId);
            int result = run_child(write_fd[childId], read_fd[childId], (local_id)childId);
            suicide();
            exit(result);
        }
    }
    //Parent logic here
    return run_parent();
}

void close_unused_fd(local_id process_id)
{
    for (int i=0; i< connections_count; i++)
    {
        if (i == process_id) {continue;}
        for (int j=0; j < connections_count; j++)
        {
            if (i == j) {continue;}
            close(read_fd[i][j]);
            close(write_fd[i][j]);
        }
    }
}

int run_parent()
{
    int result = SUCCESS;
    int child_status;
    //close other FD
    close_unused_fd(PARENT_ID);

    MessageAttributes attr;
    //Creating message
    Message msg;
    attr.write_fd = write_fd[PARENT_ID];
    attr.read_fd = read_fd[PARENT_ID];
    attr.processes_count = processes_count;
    attr.id = PARENT_ID;
    int started_count = 0;
    int done_count = 0;
    
    //waiting for all started
    do
    {
        receive_any(&attr, &msg);
        if (msg.s_header.s_type == STARTED)
        {
            started_count++;
        }
    }
    while (started_count < processes_count);

    //all started event logging
    char message[MAX_PAYLOAD_LEN];
    timestamp_t time = get_physical_time();
    sprintf(message, log_received_all_started_fmt, time, PARENT_ID);
    write(eventslog, message, strlen(message));
    printf("%s", message);

    //banking
    bank_robbery(&attr, processes_count);

    //send stop to children
    msg.s_header.s_magic = MESSAGE_MAGIC;
    msg.s_header.s_type = STOP;
    msg.s_header.s_local_time = get_physical_time();
    msg.s_header.s_payload_len = 0;
    send_multicast(&attr, &msg);

    int sender_id;
    //waiting for all done
    do
    {
        sender_id = receive_any(&attr, &msg);
        if (msg.s_header.s_type == DONE)
        {
            done_count++;
        }
    }
    while (done_count < processes_count);
    time = get_physical_time();
    sprintf(message, log_received_all_done_fmt, time, PARENT_ID);
    write(eventslog, message, strlen(message));
    printf("%s", message);

    //getting history
    AllHistory allHistory;
    allHistory.s_history_len = processes_count;
    int history_count = 0;
    do
    {
        sender_id = receive_any(&attr, &msg);
        if (msg.s_header.s_type == BALANCE_HISTORY)
        {
            memcpy(&allHistory.s_history[sender_id-1],msg.s_payload, sizeof(BalanceHistory)); 
            history_count++;
        }
    }
    while (history_count < processes_count);
    print_history(&allHistory);
    //waiting for childrens death
    do
    {
        waitpid(ANY_CHILD, &child_status, 0);
        if (SUCCESS != child_status) 
        {
            result = FAILURE;
        }
        processes_count--;
    }
    while (processes_count > 0);
    suicide();
    return result;
}



int run_child(int* write_fd, int* read_fd, local_id child_id)
{
    synchronize(write_fd, read_fd, child_id, STARTED);
    do_transactions(write_fd, read_fd, child_id);
    return SUCCESS;
}

int synchronize(int* write_fd, int* read_fd, local_id child_id, MessageType m_type)
{
    if (NULL == write_fd || NULL == read_fd) { return FAILURE; }

    MessageAttributes attr;
    //Creating message
    Message msg;
    msg.s_header.s_local_time = get_physical_time();
    msg.s_header.s_magic = MESSAGE_MAGIC;
    msg.s_header.s_type = m_type;
    attr.write_fd = write_fd;
    attr.read_fd = read_fd;
    attr.processes_count = processes_count;
    attr.id = child_id;

    if (STARTED == m_type)
    {
        sprintf(msg.s_payload, log_started_fmt, get_physical_time(), child_id, getpid(), getppid(), balance);   
    }
    else if (DONE == m_type)
    {
        sprintf(msg.s_payload, log_done_fmt, get_physical_time(), child_id, balance); 
    }
    msg.s_header.s_payload_len = strlen(msg.s_payload);

    send_multicast(&attr, &msg);
    write(eventslog, msg.s_payload, msg.s_header.s_payload_len);
    printf("%s", msg.s_payload);

    //Finish of synchronisation
    for (int i=0; i<processes_count-1; ++i)
    {
        receive_any(&attr, &msg);
    }
    if (STARTED == m_type)
    {
        sprintf(msg.s_payload, log_received_all_started_fmt, get_physical_time(), child_id); 
        write(eventslog, msg.s_payload, strlen(msg.s_payload));
        printf("%s", msg.s_payload);
    }
    else if (DONE == m_type)
    {
        sprintf(msg.s_payload, log_received_all_done_fmt, get_physical_time(), child_id);
        write(eventslog, msg.s_payload, strlen(msg.s_payload));
        printf("%s", msg.s_payload);
    }
    return SUCCESS;
}

int do_transactions(int* write_fd, int* read_fd, local_id child_id)
{
    Message msg;
    TransferOrder t_order;
    MessageAttributes attr;
    
    attr.write_fd = write_fd;
    attr.read_fd = read_fd;
    attr.processes_count = processes_count;
    attr.id = child_id;
    
    while(1)
    {
        receive_any(&attr, &msg);
        timestamp_t current_time = get_physical_time();
        //Filling in history to current moment
        for (uint8_t i = history.s_history_len; i <= current_time; i++)
        {
            history.s_history[i].s_balance = balance;
            history.s_history[i].s_balance_pending_in = 0;
            history.s_history[i].s_time = i;
        }
        history.s_history_len = current_time + 1;
        if (TRANSFER == msg.s_header.s_type)
        {
            memcpy(&t_order, &msg.s_payload, msg.s_header.s_payload_len);
            //I must send
            if (child_id == t_order.s_src) 
            {
                //Logging of log_transfer_out_fmt
                char message[MAX_PAYLOAD_LEN];
                sprintf(message, log_transfer_in_fmt, get_physical_time(), child_id, t_order.s_amount, t_order.s_dst); 
                write(eventslog, message, strlen(message));
                printf("%s", message);

                balance -= t_order.s_amount;
                msg.s_header.s_local_time = current_time;
                send(&attr, t_order.s_dst, &msg);
            }
            //I must receive
            else 
            {
                //Logging of log_transfer_in_fmt
                char message[MAX_PAYLOAD_LEN];
                sprintf(message, log_transfer_out_fmt, get_physical_time(), child_id, t_order.s_amount, t_order.s_src); 
                write(eventslog, message, strlen(message));
                printf("%s", message);

                balance += t_order.s_amount;
                msg.s_header.s_local_time = current_time;
                msg.s_header.s_magic = MESSAGE_MAGIC;
                msg.s_header.s_type = ACK;
                msg.s_header.s_payload_len = 0;
                send(&attr, 0, &msg);
            }
            //Fixing last history balance record
            history.s_history[current_time].s_balance = balance;
        }
        else if (STOP == msg.s_header.s_type)
        {
            //FIXME
            synchronize(write_fd, read_fd, child_id, DONE);
            Message history_msg;
            history_msg.s_header.s_local_time = get_physical_time();
            history_msg.s_header.s_type = BALANCE_HISTORY;
            ssize_t size = sizeof(BalanceHistory) - sizeof(BalanceState)*(MAX_T + 1 - history.s_history_len);
            memcpy(history_msg.s_payload, &history, size);
            history_msg.s_header.s_payload_len = size;
            send(&attr, 0, &history_msg);
            return 0;
        }
    }
    return 0;
}

void suicide()
{
    free_descriptors();
    close(eventslog);
}

void free_descriptors()
{
    close_descriptors();
    for (int i=0; i<connections_count; ++i)
    {
        free(write_fd[i]);
        free(read_fd[i]);
    }
    free(read_fd);
    free(write_fd);
}

void close_descriptors()
{
    for (int i=0; i<connections_count ; ++i)
    {
        for (int j=0; j < connections_count; ++j)
        {
            close(read_fd[i][j]);
            close(write_fd[i][j]);
        }
    }

}

