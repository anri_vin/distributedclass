#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include "ipc.h"
#include "child.h"
#include <time.h>
#include <sys/types.h>
#include <string.h>
#include "banking.h"

#define _BSD_SOURCE
#define TIME_OUT 5000

int usleep(long usec);

int send(void *self, local_id dst, const Message *msg)
{
    int dst_fd;
    MessageAttributes* attr;
    attr = (MessageAttributes*)(self);
    dst_fd = attr->write_fd[(int)(dst)];
    write(dst_fd, msg, sizeof(MessageHeader) + msg->s_header.s_payload_len);
    return 0;
}

int send_multicast(void *self, const Message *msg)
{
    MessageAttributes *attr;
    attr = (MessageAttributes*)self;
    for (int i = 0; i < attr->processes_count+1; i++)
    {
        if (i == attr->id) {continue;}
        write(attr->write_fd[i], msg, sizeof(MessageHeader) + msg->s_header.s_payload_len);
    }
    return 0;
}

int receive(void *self, local_id from, Message *msg)
{
    int rcv_fd;
    int readed = 0;
    MessageAttributes *attr;
    attr = (MessageAttributes*)self;
    rcv_fd = attr->read_fd[(int)(from)];
    do
    {
        ssize_t len = read(attr->read_fd[(int)(from)], msg, sizeof(Message));
        if (len == -1 && errno == EAGAIN)
        {
            usleep(TIME_OUT);
            continue;
        }
        else
        {
            readed = 1;
            break;
        }
    } while(!readed);
    return 0;
}

int receive_any(void *self, Message *msg)
{
    int readed = 0;
    int process_id = -1;
    MessageAttributes *attr;
    attr = (MessageAttributes*)self;
    do
    {
        for (int i = 0; i < attr->processes_count+1; i++)
        {
            if (attr->id == i) { continue; }
            ssize_t len = read(attr->read_fd[i], msg, sizeof(Message));
            if ((len == -1 && errno == EAGAIN) || len == 0)
            {
                continue;
            }
            else
            {
                readed = 1;
                process_id = i;
                break;
            }
        }
        usleep(TIME_OUT);
    }
    while (!readed);
    return process_id;
}
