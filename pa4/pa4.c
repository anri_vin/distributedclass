#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include "common.h"
#include "ipc.h"
#include "pa2345.h"
#include "child.h"
#include <getopt.h>
#include <sys/wait.h>
#include <time.h>
#include "pa2345.h"

#define _GNU_SOURCE 1

#define SUCCESS 0
#define FAILURE 1
#define PARENT_ID 0
#define ANY_CHILD -1
#define READ_FD 0
#define WRITE_FD 1

static int processes_count = 0;
static FILE  *pipeslog;
static int eventslog;
static int use_mutex = 0;
static int connections_count = 0;
static int** read_fd;
static int** write_fd;
extern timestamp_t lamport_time;

void close_unused_fd(local_id process_id);
int run_child(int* write_fd, int* read_fd, local_id child_id);
int synchronize(int* write_fd, int* read_fd, local_id child_id, MessageType m_type);
int do_transactions(int* write_fd, int* read_fd, local_id child_id);
void close_descriptors();
void free_descriptors();
void suicide();
int run_parent();

int pipe2(int pipefd[2], int flags);

timestamp_t get_lamport_time();
void inc_lamport_time();

int main(int argc, char **argv)
{
    char option;
    int flag_is_found = 0;
    int length = 0;
    int fields[2];
    lamport_time = 0;
    //Parsing command line arguments.
    static struct option long_options[] = {
        {"mutexl", no_argument, &use_mutex, 1},
        {"processes", required_argument, 0, 'p'},
        {0, 0, 0, 0}
    };
    while ((option = getopt_long(argc, argv, "p:", long_options, NULL)) != -1)
    {
       	switch (option)
      	{
            case 0 :
                break;
            case 'p':
            	length = strlen(optarg);
                for (int i = 0; i < length; i++)
                if (!isdigit(optarg[i]))
                {
                     exit(1);
                }
                processes_count = atoi(optarg);
                flag_is_found = 1;
                if (processes_count > 10 || processes_count < 1)
                {
                    exit(1);
                }
                break;
            case '?': 
                exit(1);
        }
    }
    //Exiting if -p flag not found
    if (!flag_is_found)
    {
        exit(FAILURE);
    }
    //Creating files to logging
    eventslog = open(events_log, O_CREAT | O_TRUNC |  O_RDWR | O_APPEND, S_IRWXU | S_IRWXG | S_IRWXO);
    pipeslog = fopen(pipes_log, "w");

    //Creating files descriptors
    connections_count = processes_count + 1;
    read_fd = malloc(sizeof(int*)*(connections_count));
    write_fd = malloc(sizeof(int*)*(connections_count));
    for (int i=0; i<connections_count; ++i)
    {
        read_fd[i] = malloc(sizeof(int)*(connections_count));
        write_fd[i] = malloc(sizeof(int)*(connections_count));
    }

    //Creating files descriptors
    for (int i=0; i<connections_count; ++i)
    {
        for (int j=0; j<connections_count; ++j)
        {
            if (i == j) { continue; }
            if (SUCCESS == pipe2(fields, O_NONBLOCK))
            {
		read_fd[j][i] = fields[READ_FD];
                write_fd[i][j] = fields[WRITE_FD];
                fprintf(pipeslog, "%d-->%d %d/%d\n", i,j, fields[WRITE_FD], fields[READ_FD]);
            }
            else
            {
                free_descriptors();
                exit(FAILURE);
            }
        }
    }
    //Saving pipes to log
    fclose(pipeslog);

    //init lamport time
    lamport_time = 0;

    //Starting to create children
    pid_t *child_processes = malloc(sizeof(pid_t) * processes_count);
    for (int i = 0; i < processes_count; i++)
    {
        child_processes[i] = fork();
        int childId = i + 1;
        //Exit if failed to fork
        if (-1 == child_processes[i] )
        {
                free_descriptors();
                exit(FAILURE);
        }
        //Child logic here
        if (0 ==  child_processes[i])
        {
            //close other child FD
            close_unused_fd((local_id)childId);
            int result = 0; 
            if (use_mutex != 1)
            {
                run_child(write_fd[childId], read_fd[childId], (local_id)childId);
            }
            suicide();
            exit(result);
        }
     }
     for (int i=0; i< processes_count; ++i)
     {
        int childId = i+ 1;
        if (use_mutex)
        {
            int count = childId * 5;
            for (int i = 1; i <= count; i++) 
            {
                     char messageInfo[MAX_PAYLOAD_LEN];
                     sprintf(messageInfo, log_loop_operation_fmt, childId, i, count);
                     print(messageInfo);
            }
        }
    }
    //Parent logic here
    if (use_mutex)
    {   
        suicide();
        return 0;
    }
    else
    return run_parent();
}

void close_unused_fd(local_id process_id)
{
    for (int i=0; i< connections_count; i++)
    {
        if (i == process_id) {continue;}
        for (int j=0; j < connections_count; j++)
        {
            if (i == j) {continue;}
            close(read_fd[i][j]);
            close(write_fd[i][j]);
        }
    }
}

int run_parent()
{
    int result = SUCCESS;
    int child_status;
    //close other FD
    close_unused_fd(PARENT_ID);

    MessageAttributes attr;
    //Creating message
    Message msg;
    attr.write_fd = write_fd[PARENT_ID];
    attr.read_fd = read_fd[PARENT_ID];
    attr.processes_count = processes_count;
    attr.id = PARENT_ID;
    int started_count = 0;
    //int done_count = 0;
    
    //waiting for all started
    do
    {
        receive_any(&attr, &msg);
        if (msg.s_header.s_type == STARTED)
        {
            started_count++;
        }
    }
    while (started_count < processes_count);

    //all started event logging
    char message[MAX_PAYLOAD_LEN];
    timestamp_t time = get_lamport_time();
    sprintf(message, log_received_all_started_fmt, time, PARENT_ID);
    write(eventslog, message, strlen(message));
    printf("%s", message);

    //waiting for childrens death
    do
    {
        waitpid(ANY_CHILD, &child_status, 0);
        if (SUCCESS != child_status) 
        {
            result = FAILURE;
        }
        processes_count--;
    }
    while (processes_count > 0);
    suicide();
    return result;
}



int run_child(int* write_fd, int* read_fd, local_id child_id)
{
    synchronize(write_fd, read_fd, child_id, STARTED);
    {
        do_transactions(write_fd, read_fd, child_id);
    }
    synchronize(write_fd, read_fd, child_id, DONE);
    return SUCCESS;
}

int synchronize(int* write_fd, int* read_fd, local_id child_id, MessageType m_type)
{
    if (NULL == write_fd || NULL == read_fd) { return FAILURE; }

    MessageAttributes attr;
    //TODO
    //Creating message
    Message msg;
    msg.s_header.s_magic = MESSAGE_MAGIC;
    msg.s_header.s_type = m_type;
    attr.write_fd = write_fd;
    attr.read_fd = read_fd;
    attr.processes_count = processes_count;
    attr.id = child_id;

    if (STARTED == m_type)
    {
        sprintf(msg.s_payload, log_started_fmt, get_lamport_time(), child_id, getpid(), getppid(), 0);   
    }
    else if (DONE == m_type)
    {
        sprintf(msg.s_payload, log_done_fmt, get_lamport_time(), child_id, 0); 
    }
    msg.s_header.s_payload_len = strlen(msg.s_payload);

    send_multicast(&attr, &msg);
    write(eventslog, msg.s_payload, msg.s_header.s_payload_len);
    printf("%s", msg.s_payload);

    //Finish of synchronisation
    for (int i=0; i<processes_count-1; ++i)
    {
        receive_any(&attr, &msg);
	if (msg.s_header.s_type != m_type)
		i--;
    }

    if (STARTED == m_type)
    {
        sprintf(msg.s_payload, log_received_all_started_fmt, get_lamport_time(), child_id); 
        write(eventslog, msg.s_payload, strlen(msg.s_payload));
        printf("%s", msg.s_payload);
    }
    else if (DONE == m_type)
    {
        sprintf(msg.s_payload, log_received_all_done_fmt, get_lamport_time(), child_id);
        write(eventslog, msg.s_payload, strlen(msg.s_payload));
        printf("%s", msg.s_payload);
    }
    return SUCCESS;
}

int do_transactions(int* write_fd, int* read_fd, local_id child_id)
{
    
    int count = child_id * 5;
    for (int i = 1; i <= count; i++) 
    {
        if (use_mutex)
        {
            request_cs(write_fd);
        }
        char messageInfo[MAX_PAYLOAD_LEN];
        sprintf(messageInfo, log_loop_operation_fmt, child_id, i, count);
        print(messageInfo);
        if (use_mutex)
        {
            release_cs(read_fd);
        }
    }

    return 0;
}


void suicide()
{
    free_descriptors();
    close(eventslog);
}

void free_descriptors()
{
    close_descriptors();
    for (int i=0; i<connections_count; ++i)
    {
        free(write_fd[i]);
        free(read_fd[i]);
    }
    free(read_fd);
    free(write_fd);
}

void close_descriptors()
{
    for (int i=0; i<connections_count ; ++i)
    {
        for (int j=0; j < connections_count; ++j)
        {
            close(read_fd[i][j]);
            close(write_fd[i][j]);
        }
    }

}

