#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include "ipc.h"
#include "child.h"
#include <time.h>
#include <sys/types.h>
#include <string.h>
#include "banking.h"

#define _BSD_SOURCE
#define TIME_OUT 5001

timestamp_t lamport_time;

timestamp_t get_lamport_time()
{
    return lamport_time;
}

void inc_lamport_time()
{
    lamport_time++;
}

int usleep(long usec);

int send(void *self, local_id dst, const Message *msg)
{
    int dst_fd;
    MessageAttributes* attr;
    attr = (MessageAttributes*)(self);
    dst_fd = attr->write_fd[(int)(dst)];
    inc_lamport_time();
    Message message;
    message = *msg;
    message.s_header.s_local_time = get_lamport_time();
    while ( write(dst_fd, &message, sizeof(MessageHeader) + message.s_header.s_payload_len) < 0 )
    {
        if (errno != EAGAIN && errno != EWOULDBLOCK)
        {
            return -1;
        }
    }
//    usleep(1000);
    return 0;
}

int send_multicast(void *self, const Message *msg)
{
    MessageAttributes *attr;
    attr = (MessageAttributes*)self;
    inc_lamport_time();
    Message message;
    message = *msg;
    message.s_header.s_local_time = get_lamport_time();
    for (int i = 0; i < attr->processes_count+1; i++)
    {
        if (i == attr->id) {continue;}
        if (write(attr->write_fd[i], &message, sizeof(MessageHeader) + message.s_header.s_payload_len) < 0)
        {
            if (errno != EAGAIN && errno != EWOULDBLOCK)
            {
                return -1;
            }
        }
       // usleep(1000);
    }
    return 0;
}

int receive(void *self, local_id from, Message *msg)
{
    int rcv_fd;
    int readed = 0;
    MessageAttributes *attr;
    attr = (MessageAttributes*)self;
    rcv_fd = attr->read_fd[(int)(from)];
    do
    {
        switch ( read(attr->read_fd[(int)(from)], &(msg->s_header), sizeof(MessageHeader)))
        {
            case -1:
                if (EAGAIN == errno)
                {
                   // printf("Empty\n");
                  // usleep(TIME_OUT);
                }
                else
                {
                 //   printf("Fail\n");
                }
                break;
            case 0:
               // printf("Closed\n");
                break;
                break;
            default:
                if (msg->s_header.s_payload_len !=  0)
                {
                    read(attr->read_fd[(int)(from)], &(msg->s_payload), msg->s_header.s_payload_len);
                }
                readed = 1;
                break;
        }
        /*
        if ((len == -1 && errno == EAGAIN) || len == 0)
        {
            //usleep(TIME_OUT);
            continue;
        }
        else
        {
            if (msg->s_header.s_payload_len != 0)
            {
               read(attr->read_fd[(int)(from)], &(msg->s_payload), msg->s_header.s_payload_len);
            }
            readed = 1;
            break;
        }*/
    } while(!readed);

    timestamp_t received_time = msg->s_header.s_local_time;
    if(received_time > lamport_time)
    {
	lamport_time = received_time+1;		
    }
    else
    {
	++lamport_time;
    }

    return 0;
}

int receive_any(void *self, Message *msg)
{
    int readed = 0;
    int process_id = -1;
    MessageAttributes *attr;
    attr = (MessageAttributes*)self;
    do
    {
        for (int i = 0; i < attr->processes_count+1; i++)
        {
            if (attr->id == i) { continue; }
            switch (read(attr->read_fd[i], &(msg->s_header), sizeof(MessageHeader)))
            {
                case -1:
                    if (EAGAIN == errno)
                    {
                       // printf("Empty\n");
                       usleep(TIME_OUT);
                    }
                    else
                    {
                       // printf("Fail\n");
                    }
                    break;
                case 0:
                   // printf("Closed\n");
                    continue;
                    break;
                default:
                    if (msg->s_header.s_payload_len !=  0)
                    {
//			printf("Reading %d from pipe\n", msg->s_header.s_payload_len);
			if (msg->s_header.s_payload_len != 0) {
                            read(attr->read_fd[i], &(msg->s_payload), msg->s_header.s_payload_len);
                        }
		    }
                    readed = 1;
                    process_id = i;
                    i = attr->processes_count + 2;
                    break;
            }
            /*
            if ((len == -1 && errno == EAGAIN) || len == 0)
            {
                //usleep(TIME_OUT);
                continue;
            }
            else
            {
                if (msg->s_header.s_payload_len !=  0)
                {
                    read(attr->read_fd[i], &(msg->s_payload), msg->s_header.s_payload_len);
                }
                readed = 1;
                process_id = i;
                break;
            }*/
        }
    }
    while (!readed);
    timestamp_t received_time = msg->s_header.s_local_time;
    if(received_time > lamport_time)
    {
	lamport_time = received_time+1;		
    }
    else
    {
	++lamport_time;
    }
    return process_id;
}
